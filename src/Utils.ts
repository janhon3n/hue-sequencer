// Promise timeout function
export function wait(time: number): Promise<void> {
  return new Promise(resolve => {
    setTimeout(() => resolve(), time)
  })
}

export function hslToCSS(hsl: {h: number, s: number, l: number}) {
  return 'hsl(' + hsl.h + ', ' + hsl.s*100 + '%, ' + hsl.l*100 + '%)'
}