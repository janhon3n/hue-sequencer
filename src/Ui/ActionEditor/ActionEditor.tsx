import React from 'react'
import BlinkActionEditor from './BlinkActionEditor'
import Action, { ActionType } from '../../Logic/Action';
import Input from '../Input';

interface Props {
  action: Action | null,
  onEdit: (action: Action) => void
  style: any
}

interface State {
}

const defaultAction: Action = {
  type: ActionType.Blink,
  tickCount: 1,
  states: [
    { h: 1, s: 1, l: 1 }
  ]
}

export default class ActionEditor extends React.Component<Props, State> {

  componentDidUpdate() {
    if (this.props.action === null) {
      this.props.onEdit(defaultAction)
    }
  }


  render() {
    let { action, onEdit, style } = this.props

    if (!action) return null

    return <section style={style}>
      <Input label={'Action type'}>
        <select
          value={action.type}
          onChange={(event) => {
            let newAction = Object.assign({}, action, { type: event.target.value })
            if (newAction.type === ActionType.None) newAction.states = []
            onEdit(newAction)
          }}>
          {Object.keys(ActionType).map(type => {
            return <option value={type}>{type}</option>
          })}
        </select>
      </Input>
      {
        action.type === ActionType.Blink ?
          <BlinkActionEditor action={action} onEdit={onEdit} /> :
          null
      }
    </section>
  }
}