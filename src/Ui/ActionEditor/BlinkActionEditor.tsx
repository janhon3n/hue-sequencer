import React from 'react'
import { ChromePicker } from 'react-color';
import Action from '../../Logic/Action';
import Input from '../Input';

interface Props {
  action: Action
  onEdit: (action: Action) => void
}

interface State {
}

export default class BlinkActionEditor extends React.Component<Props, State> {
  render() {
    let { action, onEdit } = this.props
    return (
      <section>
        <Input label={'Blink color'}>
        <ChromePicker 
          color={action.states[0]}
          onChangeComplete={(color) => {
            let newAction = Object.assign({}, action, {states: [color.hsl]})
            onEdit(newAction)
        }} />
        </Input>
      </section>
    )
  }
}