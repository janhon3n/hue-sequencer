import React from 'react'
import LogicApi from '../../Logic/LogicApi'
import LightSetting from './LightSetting'
import LightSetup, { LightSetupState } from '../../Logic/LightService/LightSetup';

interface Props {
}

interface State {
  lightSetupState: LightSetupState | null
}

export default class Setup extends React.Component<Props, State> {

  lightSetup: LightSetup | null = null

  constructor(props: Props) {
    super(props)

    this.updateLightSetupState = this.updateLightSetupState.bind(this)

    this.state = {
      lightSetupState: null
    }
  }

  async componentDidMount() {
    let logic = await LogicApi.getLogic()
    this.lightSetup = logic.getLightService().getLightSetup()
    this.lightSetup.subscribeToState(this.updateLightSetupState)

  }
  componentWillUnmount() {
    this.lightSetup && this.lightSetup.unsubscribeToState(this.updateLightSetupState)
  }

  updateLightSetupState(state: LightSetupState) {
    this.setState({ lightSetupState: state })
  }

  render() {
    let {lightSetupState} = this.state

    if (!lightSetupState) return <section>Loading light setup...</section>

    return <section style={{ padding: 20, backgroundColor: '#333', width: 250 }}>
      <h2>Light setup</h2>
      {
        lightSetupState.lights.map(light => {
          if(!lightSetupState) return null
            let mapping = lightSetupState.laneMapping.find(ls => ls.lightId === light.id)
          
          return <LightSetting light={light} laneIndex={mapping ? mapping.laneIndex : undefined} />
        })
      }
    </section>
  }
}