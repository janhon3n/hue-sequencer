import React from 'react'
import LogicApi from '../../Logic/LogicApi'
import { MdArrowForward, MdArrowBack } from 'react-icons/lib/md';
import Light from '../../Logic/LightService/Light';

interface Props {
  light: Light
  laneIndex?: number
}

interface State {
}

export default class extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props)
  }

  render() {
    return <section style={{
      flexDirection: 'row',
      justifyContent: 'space-between',
      margin: 10,
      marginBottom: 0
    }}>
      {this.props.light.name}
      <div>
        <MdArrowBack />
        {this.props.laneIndex}
        <MdArrowForward />
      </div>
    </section>
  }
}