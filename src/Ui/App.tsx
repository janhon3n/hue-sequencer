import React from 'react'
import SequencerView from './SequencerView/SequencerView'
import Setup from './Setup/Setup'
import LogicApi from '../Logic/LogicApi'

interface Props {
}

interface State {
  logicReady: boolean
}

class App extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props)

    this.state = {
      logicReady: false
    }
  }

  async componentDidMount() {
    await LogicApi.getLogic()
    this.setState({logicReady: true})
  }

  render() {
    if (!this.state.logicReady) {
      return <section>Initializing logic...</section>
    }

    return (
      <section>
        <h1>Hue sequencer</h1>
        <section style={{ flexDirection: 'row' }}>
          <Setup />
          <SequencerView />
        </section>
      </section>
    );
  }
}

export default App;
