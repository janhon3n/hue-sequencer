import React, { ReactElement } from "react";

export default function(props: {
  label: string
  children: ReactElement
}) {
  return <div style={{margin: 5}}>
    <div style={{fontSize: '0.8em', marginBottom: 2}}>{props.label}</div>
    {props.children}
  </div>
}