import _ from 'underscore'
import React from 'react'
import Sequencer, { SequencerState } from '../../Logic/Sequencer'
import LogicApi from '../../Logic/LogicApi'
import SequencerLane from './SequencerLane'
import PlayControls from './PlayControls'
import ActionEditor from '../ActionEditor/ActionEditor'
import Action, { ActionPointer } from '../../Logic/Action'


interface Props {
}

interface State {
  state: SequencerState | null
  actionUnderEditPointer: ActionPointer | null
}

export default class SequencerView extends React.Component<Props, State> {

  sequencer: Sequencer | null = null

  constructor(props: Props) {
    super(props)

    this.updateSequencerState = this.updateSequencerState.bind(this)

    this.state = {
      state: null,
      actionUnderEditPointer: null,
    }
  }

  async componentDidMount() {
    let logic = await LogicApi.getLogic()
    this.sequencer = logic.getSequencer()
    this.sequencer.subscribeToState(this.updateSequencerState)
  }

  componentWillUnmount() {
    this.sequencer && this.sequencer.unsubscribeToState(this.updateSequencerState)
  }

  updateSequencerState(state: SequencerState) {
    this.setState({state})
  }


  getAction(pointer: ActionPointer) {
    return this.state.state && this.state.state.program.lanes[pointer.laneIndex].actions[pointer.actionIndex]
  }

  render() {
    if (!this.state.state || !this.sequencer) return null

    let { actionUnderEditPointer } = this.state
    let { program, currentTick, running } = this.state.state

    let actionUnderEdit = actionUnderEditPointer && this.getAction(actionUnderEditPointer)

    return (
      <section style={{ flexGrow: 1, margin: 25 }}>
        <PlayControls
          running={running}
          onPlay={() => this.sequencer && this.sequencer.run()}
          onPause={() => this.sequencer && this.sequencer.pause()}
          onStop={() => this.sequencer && this.sequencer.stop()}
          onTempoChange={(timeBetween) => {
            if (this.sequencer) this.sequencer.tempo = timeBetween
          }}
        />
        <section>
          {
            program.lanes.map((lane, laneIndex: number) => {
              return (
                <SequencerLane
                  currentTick={currentTick}
                  lane={lane}
                  laneIndex={laneIndex}
                  onActionChosenForEdit={(actionIndex: number) => {
                    this.setState({ actionUnderEditPointer: { laneIndex, actionIndex } })
                  }}
                  onActionSplit={(actionIndex: number) => {
                    this.sequencer && this.sequencer.splitAction({ laneIndex, actionIndex })
                  }} />
              )
            })
          }
        </section>
        {actionUnderEditPointer &&
          <ActionEditor
            style={{ marginTop: 30 }}
            action={actionUnderEdit}
            onEdit={(action: Action) => {
              this.sequencer &&
                actionUnderEditPointer &&
                this.sequencer.updateAction(actionUnderEditPointer, action)
            }} />}
      </section>
    )
  }
}