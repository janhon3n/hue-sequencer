import React from 'react'
import ActionMarker from './ActionMarker'
import { LaneState } from '../../Logic/Sequencer'

interface Props {
  currentTick: number
  lane: LaneState,
  laneIndex: number,
  onActionChosenForEdit: (actionIndex: number) => void
  onActionSplit: (actionIndex: number) => void
}

interface State {
}

export default class SequencerLane extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props)
  }

  isActive(actionIndex: number) {
    let { lane, currentTick } = this.props
    let ticksBeforeThisAction = 0
    lane.actions.forEach((a, i) => {
      if (i < actionIndex) ticksBeforeThisAction += a.tickCount
    })
    console.log(currentTick)
    return currentTick >= ticksBeforeThisAction && currentTick < ticksBeforeThisAction + lane.actions[actionIndex].tickCount
  }

  render() {
    let { lane, laneIndex, onActionChosenForEdit, onActionSplit } = this.props
    return (
      <section style={{ flexDirection: 'row', margin: 3 }}>
        <div style={{fontSize: '2.0em', marginRight: 10}}>{laneIndex}</div>
        {lane.actions.map((action, i: number) => {
          return <ActionMarker
            active={this.isActive(i)}
            action={action}
            onSelectForEdit={() => onActionChosenForEdit(i)}
            onSplit={() => onActionSplit(i)} />
        })}
      </section>
    )
  }
}