import React, { ReactElement } from 'react'
import {MdPlayCircleOutline as PlayI, MdPauseCircleOutline as PauseI, MdStop as StopI} from 'react-icons/lib/md';

interface Props {
  running: boolean
  onPlay: () => void
  onPause: () => void
  onStop: () => void
  onTempoChange: (timeBetween: number) => void
}

interface State {}

export default class PlayControls extends React.Component<Props, State> {

  lastTap: number | null = null

  constructor(props: Props) {
    super(props)
    this.tapTempo = this.tapTempo.bind(this)
  }

  tapTempo() {
    let now = new Date().getTime()
    if (this.lastTap) {
      let timeBetween = now - this.lastTap
      if (timeBetween > 200 && timeBetween < 5000)
        this.props.onTempoChange(timeBetween)
    }
    this.lastTap = new Date().getTime()
  }

  render() {
    let { onPlay, onPause, onStop, running } = this.props
    return <section style={{ fontSize: '1.1em', flexDirection: 'row', justifyContent: 'flex-start' }}>
        <PlayControlsButton onClick={onPlay} icon={<PlayI />} />
        <PlayControlsButton onClick={onPause} icon={<PauseI />} />
        <PlayControlsButton onClick={onStop} icon={<StopI />} />
        <PlayControlsButton onClick={this.tapTempo} icon={<StopI />} />
    </section>
  }
}

function PlayControlsButton(props: {
  onClick: () => void
  icon: ReactElement
}) {
  return <div
    style={{
      margin: 2,
      border: 'none',
      borderRadius: 15
    }}
    onClick={props.onClick}>
    {props.icon}
  </div>
}