import React from 'react'
import { hslToCSS } from '../../Utils';
import Action from '../../Logic/Action'

interface Props {
  active: boolean
  onSelectForEdit: () => void
  onSplit: () => void
  action: Action
}

interface State {
}

export default class ActionMarker extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props)
  }

  render() {
    let { action, onSelectForEdit, onSplit, active } = this.props
    let displayState = (action && action.states[0]) ? action.states[0] : { h: 0, s: 0, l: 0.8 }

    let width = 70 * action.tickCount

    return (
      <section style={{ width }}>
        <div
          onClick={onSelectForEdit}
          style={{
            cursor: 'pointer',
            border: '3px solid ' + (active ? 'white' : '#222'),
            marginRight: 10,
            backgroundColor: hslToCSS(displayState),
            height: 35,
          }}
        >
          {action.tickCount > 1 ? <button onClick={onSplit}>Split</button> : null}
        </div>
      </section>
    )
  }
}