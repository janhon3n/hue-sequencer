export interface LightState {
  h: number
  s: number
  l: number
}


export interface LightInterface {
  id: string,
  name: string,
  state: LightState
  setState(state: LightState): void
}

export default class Light implements LightInterface {
  id: string
  name: string
  lastUpdate: number

  static OFF: LightState = {
    h: 0, s: 0, l: 0
  }

  state = Light.OFF

  constructor(id: string, name: string) {
    this.id = id
    this.name = name
    this.lastUpdate = new Date().getTime()
  }

  setState(state: LightState, changeTime?: number) {
    this.state = state
    let timeBetween = new Date().getTime() - this.lastUpdate
    this.lastUpdate = new Date().getTime()
    this.onStateChanged(timeBetween, changeTime || 400)
  }

  // for implementations to transmit the state forward
  protected onStateChanged(timeBetween: number, changeTime: number) {
  }
}