import LightService from './LightService'
import axios from 'axios'
import HueLight from './HueLight'
import Light, { LightState } from './Light';
import LightSetup from './LightSetup';

const HUE_USERNAME = "q0HcOzYKStclaKT0AOM2T-ee280bimWRmx99ozUN"
const HUE_API_URL = "http://192.168.123.100/api/" + HUE_USERNAME + "/"

interface HueLightState {
  [key: string]: {
    name: string
  }
}

export default class HueLightService implements LightService {

  ready = false
  lights: HueLight[] | null = null

  async init() {
    console.log('Initializing HueLightService...')
    let lightData = await axios.get<HueLightState>(HUE_API_URL + "lights")
    this.lights = Object.keys(lightData.data).map(key => {
      return new HueLight(key, lightData.data[key].name)
    })
    this.ready = true
    console.log('HueLightService ready')
  }

  getLightSetup() {
    if(!this.lights) throw Error('Service is not initialized')
    return new LightSetup(this.lights)
  }
  
  getLights(): HueLight[] {
    if(!this.lights) throw Error('Service is not initialized')
      return this.lights
  }

  static async sendLightStateCommand(id: string, state: LightState, changeTime: number) {
    console.log("sending command to lamp:", state)

    let sat = Math.floor(state.s * 254)
    let bri = Math.floor(state.l * 254)
    let hue = Math.floor(state.h * 182)

    var body
    if (bri === 0) {
      body = {
        "on": false,
      }
    } else {
      body = {
        "on": true,
        "sat": sat,
        "bri": bri,
        "hue": hue,
        "transitiontime": Math.round(changeTime / 100),
      }
    }
    await axios.put(HUE_API_URL + "lights/" + id + "/state", body)
  }
}