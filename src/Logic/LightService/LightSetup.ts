import Stateful from "../Stateful";
import Light from "./Light";

export interface LightSetupState {
  lights: Light[]
  laneMapping: {
    laneIndex: number,
    lightId: string
  }[]
}

export default class LightSetup extends Stateful<LightSetupState> {

  lights: Light[]

  laneMapping: {
    lightId: string
    laneIndex: number
  }[]

  constructor(lights: Light[]) {
    super()
    this.lights = lights
    this.laneMapping = lights.map((light, i) => {
      return {
        lightId: light.id,
        laneIndex: i,
      }
    })
    this.updateState()
  }

  getState(): LightSetupState {
    return {
      lights: this.lights,
      laneMapping: this.laneMapping
    }
  }

  getLightsForLane(index: number): Light[] {
    let lightIds = this.laneMapping
      .filter(({ laneIndex }) => laneIndex === index)
      .map((lm => lm.lightId))

    return this.lights.filter(l => lightIds.includes(l.id))
  }
}