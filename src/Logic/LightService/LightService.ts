import LightSetup from './LightSetup'

export default interface LightService {
  ready: boolean
  init: () => Promise<void>
  getLightSetup: () => LightSetup
}