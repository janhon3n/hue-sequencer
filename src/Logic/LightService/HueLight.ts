import Light, {LightState} from './Light'
import HueLightService from './HueLightService'

export default class HueLight extends Light {
  protected onStateChanged(timeBetween: number, changeTime: number) {
    HueLightService.sendLightStateCommand(this.id, this.state, changeTime)
  }
}