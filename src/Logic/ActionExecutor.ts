import Light from "./LightService/Light"
import Action, { ActionType } from "./Action"
import { wait } from "../Utils"

interface TempoSource {
  getTempo: () => number
}

export default class ActionExecutor {

  tempoSource: TempoSource

  constructor(programSource: TempoSource) {
    this.tempoSource = programSource
  }

  executeAction(light: Light, action: Action): void {
    if (action.type === ActionType.Blink)
      this.executeBlinkAction(light, action)
    else if (action.type === ActionType.Fade)
      this.executeFadeAction(light, action)
  }

  private async executeBlinkAction(light: Light, action: Action): Promise<void> {
    light.setState(action.states[0])
    await wait(200)
    light.setState(Light.OFF)
  }

  private async executeFadeAction(light: Light, action: Action): Promise<void> {
    let fadeTime = action.tickCount * this.tempoSource.getTempo()
    light.setState(action.states[0], fadeTime)
  }
}

