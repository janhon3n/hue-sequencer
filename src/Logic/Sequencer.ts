import _ from 'underscore'
import Action, { ActionPointer, ActionType } from './Action'
import { wait } from '../Utils'
import ActionExecutor from './ActionExecutor'
import Stateful from './Stateful'
import LightService from './LightService/LightService'

export interface LaneState {
  actions: Action[]
}

export interface Program {
  ticks: number
  lanes: LaneState[]
}

const emptyProgram = {
  ticks: 4,
  lanes: _.range(2).map(() => {
    return {
      actions: [{
        type: ActionType.None,
        tickCount: 4,
        states: [],
      }]
    }
  }),
}

export interface SequencerState {
  program: Program,
  running: boolean,
  currentTick: number,
}

export default class Sequencer extends Stateful<SequencerState> {
  program: Program
  lightService: LightService
  actionExecutor: ActionExecutor

  stopRequested: boolean = false
  pauseRequested: boolean = false
  running: boolean = false

  currentTick: number = 0
  tempo: number = 1000

  lastState: SequencerState

  constructor(lightService: LightService, program?: Program) {
    super()
    this.program = program || emptyProgram
    this.lightService = lightService
    this.actionExecutor = new ActionExecutor(this)
    this.lastState = this.getState()
  }

  async run() {
    if (!this.running) {
      console.log("Running program")
      this.running = true
      this.stopRequested = false
      this.pauseRequested = false
      while (!this.stopRequested && !this.pauseRequested) {
        await this.runTick()
      }
      if (this.stopRequested)
        this.currentTick = 0
      this.running = false
      this.updateState()
    }
  }

  async runTick() {
    this.program.lanes.forEach((lane, laneIndex) => {
      let action = this.getActionThatWillBeActivatedOnThisTick(lane)
      this.lightService.getLightSetup().getLightsForLane(laneIndex).forEach(light => {
        if (action && action.type !== ActionType.None)
          this.actionExecutor.executeAction(light, action)
      })
    })
    this.updateState()
    await wait(this.tempo)
    this.currentTick = (this.currentTick + 1) % this.program.ticks
  }

  pause() {
    this.pauseRequested = true
  }

  stop() {
    this.stopRequested = true
  }

  getActionThatWillBeActivatedOnThisTick(lane: LaneState): Action | null {
    let ticks = 0
    let actionIndex = 0
    while (ticks < this.currentTick) {
      ticks += lane.actions[actionIndex].tickCount
      actionIndex++
    }
    return ticks === this.currentTick ? lane.actions[actionIndex] : null
  }

  getState() {
    return {
      program: this.program,
      running: this.running,
      currentTick: this.currentTick,
    }
  }

  private getActionByPointer(pointer: ActionPointer): Action {
    return this.program.lanes[pointer.laneIndex].actions[pointer.actionIndex]
  }

  // public actions
  public updateAction(pointer: ActionPointer, action: Action): void {
    this.program.lanes[pointer.laneIndex].actions[pointer.actionIndex] = action
    this.updateState()
  }

  public updateLane(laneIndex: number, lane: LaneState): void {
    this.program.lanes[laneIndex] = lane
    this.updateState()
  }

  public splitAction(pointer: ActionPointer): void {
    let action = this.getActionByPointer(pointer)
    if (action.tickCount === 1) throw new Error('Cannot split action with tick count 1')
    let newTickCounts = Math.floor(action.tickCount / 2)

    let modifiedAction = Object.assign({}, action, { tickCount: newTickCounts })
    let newNoneAction = {
      type: ActionType.None,
      tickCount: newTickCounts,
      states: [],
    }

    let newLane: LaneState = Object.assign({}, this.program.lanes[pointer.laneIndex], { actions: [] })
    this.program.lanes[pointer.laneIndex].actions.forEach((action, i) => {
      if (i === pointer.actionIndex) {
        newLane.actions.push(modifiedAction)
        newLane.actions.push(newNoneAction)
      } else {
        newLane.actions.push(action)
      }
    })
    this.updateLane(pointer.laneIndex, newLane)
  }

  public getTempo(): number {
    return this.tempo
  }
}
