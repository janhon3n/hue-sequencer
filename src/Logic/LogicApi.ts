import Logic from './Logic'

export default class LogicApi {
  static logic: Logic | null = null

  static async getLogic() {
    if (!LogicApi.logic) {
      LogicApi.logic = new Logic()
      await LogicApi.logic.init()
    }
    return LogicApi.logic
  }
}
