import {LightState} from "./LightService/Light";

export enum ActionType {
  None = "None",
  Blink = "Blink",
  Fade = "Fade",
}

export default interface Action {
  type: ActionType
  tickCount: number
  states: LightState[]
}

export interface ActionPointer {
  laneIndex: number
  actionIndex: number
}