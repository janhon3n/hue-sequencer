export default abstract class Stateful<TState> {

  subscribers: ((state: TState) => void)[] = []
  lastState: TState | null = null

  abstract getState(): TState

  updateState() {
    let state = this.getState()
    this.lastState = state

    this.subscribers.forEach(s => {
      s(state)
    })
  }

  subscribeToState(update: (state: TState) => void) {
    this.subscribers.push(update)
    if (this.lastState) update(this.lastState)
  }

  unsubscribeToState(update: (state: TState) => void) {
    this.subscribers = this.subscribers.filter(s => {
      return s !== update
    })
  }

}
