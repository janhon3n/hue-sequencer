import LightService from './LightService/LightService'
import Sequencer from './Sequencer'
import HueLightService from './LightService/HueLightService'

export default class Logic {

  lightService: LightService | null = null
  sequencer: Sequencer | null = null

  async init() {
    console.log('Initializing Logic...')
    this.lightService = new HueLightService()
    await this.lightService.init()
    this.sequencer = new Sequencer(this.lightService)
    console.log('Logic ready')
  }

  getLightService(): LightService {
    if (!this.lightService) throw new Error('Logic not initialized')
    return this.lightService
  }

  getSequencer(): Sequencer {
    if (!this.sequencer) throw new Error('Logic not initialized')
    return this.sequencer
  }
}